# su-allauth-providers

Implementace OAuth2 Providera pro použití s informačním systémem klubu Silicon Hill pro django-allauth.

Informace k implementaci OAuth2 v IS.SH lze nalézt na stránkách <https://is.sh.cvut.cz/oauth_api>.
