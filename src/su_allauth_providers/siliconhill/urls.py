from allauth.socialaccount.providers.oauth.urls import default_urlpatterns

from .provider import SiliconHillOAuth2Provider


urlpatterns = default_urlpatterns(SiliconHillOAuth2Provider)
