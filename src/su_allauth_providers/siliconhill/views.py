import requests

from allauth.socialaccount.providers.oauth2.views import (
    OAuth2Adapter,
    OAuth2CallbackView,
    OAuth2LoginView,
)


class SiliconHillOAuth2Adapter(OAuth2Adapter):
    provider_id = "siliconhill"
    access_token_url = "https://is.sh.cvut.cz/oauth/token"
    authorize_url = "https://is.sh.cvut.cz/oauth/authorize"
    profile_url = "https://api.is.sh.cvut.cz/v1/users/me"
    redirect_uri_protocol = None

    def complete_login(self, request, app, token, **kwargs):
        extra_data = requests.get(
            self.profile_url, params={"access_token": token.token}
        )

        # This only here because of weird response from the test suite
        if isinstance(extra_data, list):
            extra_data = extra_data[0]

        return self.get_provider().sociallogin_from_response(request, extra_data.json())


oauth_login = OAuth2LoginView.adapter_view(SiliconHillOAuth2Adapter)
oauth_callback = OAuth2CallbackView.adapter_view(SiliconHillOAuth2Adapter)
