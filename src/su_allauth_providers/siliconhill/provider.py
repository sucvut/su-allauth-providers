from allauth.socialaccount.providers.base import ProviderAccount
from allauth.socialaccount.providers.oauth2.provider import OAuth2Provider

from su_allauth_providers.siliconhill.views import SiliconHillOAuth2Adapter


class SiliconHillOAuth2Account(ProviderAccount):
    pass


class SiliconHillOAuth2Provider(OAuth2Provider):
    id = "siliconhill"
    name = "Silicon Hill"
    account_class = SiliconHillOAuth2Account
    oauth2_adapter_class = SiliconHillOAuth2Adapter

    def extract_uid(self, data):
        return str(data["id"])

    def extract_common_fields(self, data):
        return dict(
            email=data.get("email"),
            first_name=data.get("first_name"),
            last_name=data.get("surname"),
        )


provider_classes = [SiliconHillOAuth2Provider]
